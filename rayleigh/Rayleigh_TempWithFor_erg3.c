#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#pragma arm section zidata="sram"
int image[144][176] ;
float noisyImage[144][176];
float filteredImage[144][176];
int initImage[144][176];
float temp[146][178];
float current[3][178];
#pragma arm section

void read_sequence(){
  FILE *infile;
  int i,j;
  if ((infile = fopen("akiyo1.y","rb")) == NULL){
         printf("Image doesn't exist\n");
         exit(-1);
  }
  for (i=0;i<144;i=i+1){
      for(j=0;j<176;j=j+1){
         image[i][j] = (int) fgetc(infile);
      }
  }
  fclose(infile);
}

void write_image(float image[144][176]){
  FILE *infile;
  int i,j;
  if ((infile = fopen ("out.raw","w+")) == NULL){
      exit(-1);
  }
  for (i=0;i<144;i=i+1){
     for(j=0;j<176;j=j+1){
        fputc(image[i][j],infile);
     }
  }
  fclose(infile);
}

void create_current(int v, float current[3][178], float temp[146][178]){
  int j;
    for (j=0;j<178;j++){
      current[0][j] = current[1][j];
      current[1][j] = current[2][j];
      current[2][j] = temp[v][j];
    }
}

int main(){

  int i,j;
  int mu = 4;
  float sigma = 0.85;
  float a,b;
  int g,p;
  int sum;
  int m = 3;
  int n = 3;
  int v = 3;

  b = 4*(sigma*sigma)/(4 - 3.14);
  a = mu - sqrt((3.14*b)/4);


  read_sequence();


  for (i=0;i<144;i++){
    for (j=0;j<176;j++){
        initImage[i][j] = image[i][j];
        g = initImage[i][j];
        p = 0;
        if (g>=a){
            p = (2*(g-a)*exp(-((g-a)*(g-a)))/b)/b;
        }

        noisyImage[i][j] = p + g;
    }
  }

  for (i=0;i<146;i++){
    for (j=0;j<178;j++){
      if ( i>0 && j>0 && i<145 && j<176 ){
        temp[i][j] = noisyImage[i][j];
      }else{
        temp[i][j] = 0;
      }
    }
  }

  // for the first set of current values
  for (j=0;j<178;j++){
    current[0][j] = temp[0][j];
    current[1][j] = temp[1][j];
    current[2][j] = temp[2][j];
  }

  do{
    for (j=1;j<=176;j++){
      sum = 0;
      for (m=0;m<3;m++){
        for (n=0;n<3;n++){
          if (m==1 && n==1){
            sum = sum + 0;
          }else{
            sum = sum + current[m][j+n-1];
          }
        }
      }
      filteredImage[v-3][j-1] = sum/(m*n - 1);
    }

    create_current(v,current,temp);
    v++;

  }while (v <=146);

  write_image(filteredImage);

  }  
